Paragon Adminkit

Since Paragron Adminkit is a contrib theme, you do not want to make changes directly to the codebase for this theme.
Instead, you want to copy the starter theme out of their contrib theme directory and into the custom theme directory
for our new projects.

Please follow the steps below to automatically create new custom themes:

1. Inside the main paragon_adminkit theme directory, you will see a starterkit theme. Copy this entire directory
into your custom themes directory.

2. Next, you will need to rename all instances of "starterkit" to whatever you want your theme to be called
(e.g. Adminkit). For now, this is a manual process, so you will need to rename any files with starterkit in the name,
as well as renaming any instances of starterkit within any files.

3. As the dist directories are not committed, you will need to generate these with the steps below:
cd into /themes/custom/[your-theme-name] and run nvm use, npm install, and npm run build.

4. Navigate to /admin/appearance to install and enable your new theme. Be sure to also set this as your admin theme.

5. Clear caches.
